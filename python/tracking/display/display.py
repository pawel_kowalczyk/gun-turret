from abc import ABC, abstractmethod
from typing import Tuple, List

class Display(ABC):
    @abstractmethod
    def update_frame(self, frame):
        pass
    @abstractmethod
    def add_text(self, text:str, position:Tuple[int], font:int, font_scale:float, color:Tuple[int]):
        pass
    @abstractmethod
    def add_rectangle(self, node1:Tuple[int], node2:Tuple[int], color:Tuple[int], thickness:int):
        pass
    @abstractmethod
    def add_circle(self, center:Tuple[int], radius:int, color:Tuple[int], thickness:int):
        pass
    @abstractmethod
    def display(self):
        pass
    @abstractmethod
    def close(self):
        pass