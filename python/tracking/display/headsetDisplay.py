from display import Display
from typing import Tuple, List

class HeadsetDisplay(Display):
    def __init__(self) -> None:
        pass

    def update_frame(self, frame):
        pass
    
    def add_text(self, text:str, position:Tuple[int], font:int, font_scale:float, color:Tuple[int]):
        pass
    
    def add_rectangle(self, node1:Tuple[int], node2:Tuple[int], color:Tuple[int], thickness:int):
        pass
    
    def add_circle(self, center:Tuple[int], radius:int, color:Tuple[int], thickness:int):
        pass
    
    def display(self):
        pass
    
    def close(self):
        pass