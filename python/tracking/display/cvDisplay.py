from display.display import Display
import cv2

class CvDisplay(Display):
    """Displays the frame and additional components using cv2"""
    def __init__(self, name:str="Frame") -> None:
        self._name = name
        self._frame = None

    def update_frame(self, frame):
        # frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        self._frame = frame

    def add_text(self, text:str, position:tuple, font:int=cv2.FONT_HERSHEY_SIMPLEX, font_scale:float=0.5, color:tuple=(255,0,0)):
        cv2.putText(self._frame, text, position, font, font_scale, color)

    def add_rectangle(self, node1:tuple, node2:tuple, color:tuple=(0,255,0), thickness:int=2):
        cv2.rectangle(self._frame, node1, node2, color, thickness)

    def add_circle(self, center:tuple, radius:int, color:tuple=(0,0,255), thickness:int=1):
        cv2.circle(self._frame, center, radius, color, thickness)

    def display(self):
        cv2.imshow(self._name, self._frame)

    def close(self):
        cv2.destroyAllWindows()

    def __del__(self):
        self.close()