#pip install opencv-python opencv-contrib-python numpy pyserial pygame

from turret import Turret
from global_variables import GlobalVariables, Controllers

def main():
    GlobalVariables.DUMMY_MODE = False
    GlobalVariables.CAMERA_PORT = 1
    GlobalVariables.COMMUNICATION_PORT = 4
    GlobalVariables.CONTROLLER = Controllers.JOYSTICK
    
    gun = Turret()
    try:
        gun.tracking_loop()
    except Exception as e:
        gun.close()
        print(f"!!!__{e}__!!!")

    del gun

if __name__ == "__main__":
    main()