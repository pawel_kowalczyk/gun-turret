from interfaces.display import Display
from typing import Tuple, List
import pygame as pg
from frame2surface import frame2surface
from enum import Enum
from dataclasses import dataclass
import colors

class PygameDisplay(Display):
    
    @dataclass
    class _GuiSet:
        CAM_MODE: str    = ""
        TRIG_MODE: str   = ""
        CORRECTIONS: str = ""
        FPS: str         = ""

        def get(self):
            return [self.CAM_MODE, self.TRIG_MODE, self.CORRECTIONS, self.FPS]
        
    def __init__(self) -> None:
        super().__init__()
        pg.init()
        pg.font.init()

        self.screen = pg.display.set_mode((640,480+50))
        self.frame = None
        self.gui_set = self._GuiSet()

        self.texts = []
        self.rectangles = []
        self.circles = []
        self.graphics = []

        self.terminated = False
        self.keys = []

    def display(self) -> None:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.terminated = True

        self.screen.fill((0,0,0))
        
        self.screen.blit(self.frame, (0,0))
        # self.screen.blit(self.cockpit_img, (0,0))
        for rect in self.rectangles:
            pg.draw.rect(self.screen, *rect)
        
        for circle in self.circles:
            pg.draw.circle(self.screen, *circle)

        for text in self.texts:
            self.screen.blit(*text)

        for graphic in self.graphics:
            pass

        pg.display.flip()

        self.texts.clear()
        self.rectangles.clear()
        self.circles.clear()
        self.graphics.clear()
        
    def gui(self, cam_mode:str, trig_mode:str, corrections:str, fps:str) -> None:
        self.add_rectangle((0,480), (640,50), color=colors.GRAY, thickness=0)
        self.add_text(f"Cam Mode: {cam_mode}", (5,480+10), font_scale=16)
        self.add_text(f"Trig Mode: {trig_mode}", (205,480+10), font_scale=16)
        self.add_text(f"Corrections: {corrections}", (405,480+10), font_scale=16)
        self.add_text(f"FPS: {fps}", (5,480+30), font_scale=16)

    def update_frame(self, frame):
        self.keys = pg.key.get_pressed()
        self.frame = frame2surface(frame)

    def add_text(self, text:str, position:Tuple[int,int], font:str = pg.font.get_default_font(), font_scale:float = 16, color:Tuple[int,int,int] = colors.BLACK) -> None:
        text_font = pg.font.Font(font, font_scale)
        text_surf = text_font.render(text, False, color)
        text_box = text_surf.get_rect()
        text_box.topleft = position
        self.texts.append([text_surf, text_box])

    def add_rectangle(self, topleft:Tuple[int,int], size:Tuple[int,int], color:Tuple[int,int,int]=colors.GREEN, thickness:int=2) -> None:
        self.rectangles.append([color, pg.Rect(topleft, size), thickness])

    def add_circle(self, center:Tuple[int,int], radius:int, color:Tuple[int,int,int]=colors.BLUE, thickness:int=0) -> None:
        self.circles.append([color, center, radius, thickness])

    def get_keys(self) -> List:
        return self.keys

    def is_key_pressed(self, key:str) -> bool:
        return self.keys[eval(f"pg.K_{key}")]

    def is_terminated(self) -> bool:
        return self.terminated
    
    def close(self) -> None:
        pg.quit()
