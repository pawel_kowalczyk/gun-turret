from controlHandlers.mouseKeyboardHandler import MouseKeyboardHandler
from controlHandlers.joystickHandler import JoystickHandler
from connections.turretCommunication import TurretCommunication
from displays.pygameDisplay import PygameDisplay
from target import Target
from droneDetector import DroneDetector
from global_variables import GlobalVariables, Controllers
from crosshair import Crosshair
from threading import Thread
from typing import List, Tuple
import numpy as np
import cv2, time
import pygame as pg
import math
from enum import Enum

class Turret:
    class _TURRET_MODES(Enum):
        MANUAL = 1
        TRACKING = 2
        DETECTION = 3

    class _TRIGGER_MODES(Enum):
        SHOOTING = 1
        SELECTION = 2

    def __init__(self) -> None:
        self.stream  = cv2.VideoCapture(GlobalVariables.CAMERA_PORT)
        self.tracker = cv2.TrackerCSRT().create()
        self.drone_detector = DroneDetector()
        self.displayer = PygameDisplay()
        self.crosshair = Crosshair()
        self.turret_controller    = None
        self.turret_communication = None
        self.target = None

        self.current_frame   = None
        self.valid_stream    = True
        self.object_selected = False
        self.loop_activeted  = False
        
        if GlobalVariables.DUMMY_MODE:
            self.turret_controller = MouseKeyboardHandler()
            self.turret_communication = TurretCommunication(0)
        else:
            if GlobalVariables.CONTROLLER == Controllers.MOUSE_KEYBOARD:
                self.turret_controller = MouseKeyboardHandler()
            
            elif GlobalVariables.CONTROLLER == Controllers.JOYSTICK:
                self.turret_controller = JoystickHandler()

            self.turret_communication = TurretCommunication(GlobalVariables.COMMUNICATION_PORT)
            
        self.selected_mode = self._TURRET_MODES.MANUAL
        self.trigger_mode  = self._TRIGGER_MODES.SELECTION

        self.camera_fov = (115, 85)
        self.loop_start_time = None
        
        self.read_thread = Thread(target=self.read_frame)

        self._calculate_screen_center()
        self._unlock_drone_detection()

    def _unlock_drone_detection(self) -> None:
        """Runs detection for one frame to eliminate loading time in runtime"""
        print("---Running detection test frame---")
        self.drone_detector.detect(self.current_frame)

    def _calculate_screen_center(self) -> None:
        _, self.current_frame = self.stream.read()
        self.screen_center = [int(x/2) for x in self.current_frame.shape[0:2]]
        
    def read_frame(self) -> None:
        while self.loop_activeted:
            self.valid_stream, self.current_frame = self.stream.read()

    def select_object_from_frame(self, bbox=None) -> None:
        # bbox = cv2.selectROI('Select Object', frame)
        if not self.object_selected:
            frame = self.current_frame

            if bbox is not None:
                y,x,_,_ = bbox
                self.tracker.init(frame, bbox)
            
            else:
                y,x = self.screen_center
                bw, bh = self.crosshair.size

                bbox = (int(x-bw/2), int(y-bh/2), bw, bh)
                print(bbox)
                self.tracker.init(frame, bbox)

            self.object_selected = True

            self.target = Target()

    def automated_control(self) -> None:

        def arduino_map(value, from_low, from_high, to_low, to_high):
            if value < from_low:
                value = from_low
            elif value > from_high:
                value = from_high
            
            return (value - from_low) * (to_high - to_low) / (from_high - from_low) + to_low
        
        x,y = self.target.get_coords()
        
        frame_h, frame_w = self.current_frame.shape[:2]
        frame_w2, frame_h2 = int(frame_w/2), int(frame_h/2)
        relative_camera_fov = tuple(math.radians(i/2) for i in self.camera_fov)

        relative_coords = ( 0.4*(x/frame_w2-1), 0.4*(y/frame_h2-1) ) 
        deg_coords = ((arduino_map(relative_coords[0], -1, 1, -np.tan(relative_camera_fov[0]), np.tan(relative_camera_fov[0]))),
                      (arduino_map(relative_coords[1], -1, 1, -np.tan(relative_camera_fov[1]), np.tan(relative_camera_fov[1]))))
        
        dt = time.time()-self.loop_start_time
        delay = GlobalVariables.LOOP_PERIOD - dt*1000
        if delay > 0:
            pg.time.wait(int(delay))

        shooting = False
        if x < 0.05 and y < 0.05:
            shooting = True
        
        x,y = [round(z,2) for z in deg_coords]
        self.displayer.gui_set.CORRECTIONS = f"x:{x}, y:{y}"
        self.turret_communication.send(-x,y, shooting)
    
    def manual_control(self) -> None:
        buttons = self.turret_controller.get_clicked_buttons()
        shooting = False
        if buttons[0]:
            if self.trigger_mode == self._TRIGGER_MODES.SHOOTING:
                shooting = True
            
            elif self.trigger_mode == self._TRIGGER_MODES.SELECTION:
                self.select_object_from_frame()
                self.selected_mode = self._TURRET_MODES.TRACKING

        elif buttons[1]:
            self.crosshair.change_size(1,0)
        elif buttons[2]:
            self.crosshair.change_size(0,1)
        elif buttons[3]:
            self.crosshair.change_size(-1,0)
        elif buttons[4]:
            self.crosshair.change_size(0,-1)

        dt = time.time()-self.loop_start_time
        delay = GlobalVariables.LOOP_PERIOD - dt*1000
        if delay > 0:
            pg.time.wait(int(delay))

        x,y = [round(z,3) for z in self.turret_controller.get_corrections()]
        self.displayer.gui_set.CORRECTIONS = f"x:{x}, y:{y}"
        self.turret_communication.send(-x, -y, shooting)

    def tracking_loop(self) -> None:
        self.loop_activeted = True

        self.read_thread.start()
        
        count = 0
        while self.loop_activeted:
            self.loop_start_time = time.time()                    

            if not self.valid_stream:
                break

            frame = self.current_frame
            self.displayer.update_frame(frame)
            self.turret_controller.update()

            if self.selected_mode == self._TURRET_MODES.MANUAL:
                self.displayer.gui_set.CAM_MODE = "Manual"
                
                self.manual_control()

            elif self.selected_mode == self._TURRET_MODES.TRACKING:

                if self.object_selected:
                    
                    # Update the tracker
                    success, bbox = self.tracker.update(frame)
                    
                    self.displayer.gui_set.CAM_MODE = "Tracking"

                    if success:
                        x, y, w, h = [int(i) for i in bbox]
                        self.target.update_coords(x+w//2, y+h//2)
                        target_x, target_y = self.target.get_coords()
                        self.displayer.add_rectangle((target_x-w//2, target_y-h//2), (w,h))
                        self.displayer.add_circle((target_x, target_y), 5, thickness=0)

                        self.automated_control()

                    else:
                        self.selected_mode = self._TURRET_MODES.MANUAL
                        self.object_selected = False
                
            elif self.selected_mode == self._TURRET_MODES.DETECTION:
                self.displayer.gui_set.CAM_MODE = "Detection"
                self.displayer.gui_set.CORRECTIONS = "x:0, y:0"

                bbox = self.drone_detector.detect(frame)
                if bbox is not None:
                    self.select_object_from_frame(bbox)
                    self.selected_mode = self._TURRET_MODES.TRACKING

            self.displayer.gui_set.TRIG_MODE = "Selection" if self.trigger_mode == self._TRIGGER_MODES.SELECTION else "Shooting"

            # Draw the bounding box around the pointer
            y,x = self.screen_center
            bw2, bh2 = [self.crosshair.size[i]//2 for i in range(2)]
            self.displayer.add_rectangle((int(x-bw2), int(y-bh2)), tuple(self.crosshair.size))


            if self.displayer.is_key_pressed('q'): 
                self.loop_activeted = False

            elif self.displayer.is_key_pressed('l'):
                self.object_selected = False
                self.selected_mode = self._TURRET_MODES.MANUAL

            elif self.displayer.is_key_pressed('d'):
                if self.object_selected:
                    print("Object is currently being tracked, lose it first [press 'l']")
                else:
                    self.selected_mode = self._TURRET_MODES.DETECTION
            
            elif self.displayer.is_key_pressed('h'):
                if isinstance(self.turret_controller, (MouseKeyboardHandler,)):
                    print(GlobalVariables.KEYBOARD_CONTROLLER_INFO)

                elif isinstance(self.turret_controller, (JoystickHandler,)):
                    print(GlobalVariables.JOYSTICK_CONTROLLER_INFO)

            if self.turret_controller.get_clicked_buttons()[5]:
                self.trigger_mode = self._TRIGGER_MODES.SHOOTING

            elif self.turret_controller.get_clicked_buttons()[6]:
                self.trigger_mode = self._TRIGGER_MODES.SELECTION
            
            end = time.time()
            count += 1
            if count >= 20:
                # print(f"time: {round(1000*(end-self.loop_start_time), 4)}")
                count = 0   
            
            self.displayer.gui_set.FPS = str(int(1/(end-self.loop_start_time)))
            self.displayer.gui(*self.displayer.gui_set.get())
            self.displayer.display()

    def close(self):
        self.stream.release()
        del self.stream
        del self.tracker
        del self.displayer
        del self.turret_communication

    def _del__(self):
        self.close()