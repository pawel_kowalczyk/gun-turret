from interfaces.connection import Connection 
from serial import (Serial, SerialException)
from sys import platform

class SerialConnection(Connection):
    def __init__(self, port_n:int, baudrate:int = 115200) -> None:
        if platform.startswith('linux'):
            self.port = f"/dev/rfcomm{port_n}"
        elif platform.startswith('win'):
            self.port = f"COM{port_n}"

        self.baudrate = baudrate
        
        self.connect()

    def connect(self) -> None:
        try:
            self.connection = Serial(self.port, self.baudrate)
        except SerialException:
            print(f"Error: Could not establish a serial connection on PORT:{self.port}.")

    def write(self, message:str) -> None:
        encoded_msg = message.encode('utf-8') 
        self.connection.write(encoded_msg)

    def read(self, length:int=100) -> str:
        return self.connection.read(length)
    
    def close_connection(self) -> None:
        self.connection.close()

    def __del__(self) -> None:
        self.close_connection()

