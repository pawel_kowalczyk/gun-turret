from interfaces.connection import Connection 
from serial import (Serial, SerialException)
from sys import platform

class SerialDummyConnection(Connection):
    def __init__(self, port_n:int, baudrate:int = 115200) -> None:
        pass

    def connect(self) -> None:
        pass

    def write(self, message:str) -> None:
        pass

    def read(self, length:int=100) -> str:
        return 1
    
    def close_connection(self) -> None:
        pass

    def __del__(self) -> None:
        pass

