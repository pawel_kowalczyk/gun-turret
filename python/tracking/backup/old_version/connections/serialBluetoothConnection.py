from interfaces.connection import Connection 
from serial import (Serial, SerialException)
from sys import platform
import subprocess
import time

class SerialBluetoothConnection(Connection):
    def __init__(self, port_n:int, baudrate:int = 115200) -> None:
        """ 
            Linux: run "sudo rfcomm connect /dev/rfcomm{port_n} C8:F0:9E:F2:CF:3A 1 &" 
            Windows: connect to the device and select COM_ port
        """
        
        print("Establishing BluetoothSerial connection...")
        if platform.startswith('linux'):
            # subprocess.run(f"sudo rfcomm connect /dev/rfcomm{port_n} C8:F0:9E:F2:CF:3A 1 &".split(' '))
            self.port = f"/dev/rfcomm{port_n}"
        elif platform.startswith('win'):
            self.port = f"COM{port_n}"

        self.baudrate = baudrate
        
        self.connect()

    def connect(self) -> None:
        """On Windows you will get an error but ignore it"""
        try:
            self.connection = Serial(self.port, self.baudrate)
        except SerialException:
            print(f"Error: Could not establish a serial connection on PORT:{self.port}.")

    def write(self, message:str) -> None:
        encoded_msg = message.encode('utf-8') 
        # print(encoded_msg)        
        self.connection.write(encoded_msg)

    def read(self, length:int=100) -> str:
        return self.connection.read(length)
    
    def close_connection(self) -> None:
        self.connection.close()
        subprocess.run("sudo killall rfcomm".split(' '))

    def __del__(self) -> None:
        self.close_connection()

