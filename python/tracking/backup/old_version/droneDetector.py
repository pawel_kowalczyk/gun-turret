from ultralytics import YOLO
from typing import List, Tuple
from pathlib import Path

class DroneDetector:
    THRESHOLD = 0.7
    
    class _DetectionCase:
        def __init__(self, confidence, bbox) -> None:
            self.confidence = confidence
            self.bbox = bbox
            
        def __repr__(self) -> str:
            return f"Confidence: {self.confidence}, bbox: {self.bbox}"
         
    def __init__(self) -> None:
        model_path = Path('.') / 'detection_model' / 'best.pt'

        # Load a model
        self.model = YOLO(model_path)  # load a custom model
        
        self._detections = []

    def _get_highest_confidance_bbox(self) -> _DetectionCase:
        if len(self._detections) == 0:
            return None
        
        highest = self._detections[0]

        for case in self._detections:
            if case.confidence > highest.confidence:
                highest = case

        print(highest)

        return highest.bbox

    def detect(self, frame) -> List[int]:
        """
            Args:
                frame(cv2 frame) : input frame from the stream
            Returns:
                highest_confidence_bbox(List[Tuple[int,int],Tuple[int,int]]) : bounding box of a drone with the highest confidence 
        """
        results = self.model(frame)[0]

        for result in results.boxes.data.tolist():
            x1, y1, x2, y2, score, class_id = result
            x, y, w, h = [int(x) for x in [x1,y1,abs(x2-x1),abs(y2-y1)]]

            if score > DroneDetector.THRESHOLD:
                self._detections.append(self._DetectionCase(score, (x,y,w,h)))

        highest_confidence_bbox = self._get_highest_confidance_bbox()
        self._detections.clear()

        return highest_confidence_bbox