from enum import Enum

class Controllers(Enum):
    MOUSE_KEYBOARD = 0
    JOYSTICK       = 1

class GlobalVariables:
    DUMMY_MODE = False
    LOOP_PERIOD = 30 #ms
    CAMERA_PORT = 0
    COMMUNICATION_PORT = 0
    CONTROLLER = Controllers.MOUSE_KEYBOARD
    SCREEN_SIZE = (640,480)
    KEYBOARD_CONTROLLER_INFO = """
'q' - quit
'd' - enable detection mode
'l' - lose tracked object
't       1      2       3       4       5       6'
'trig    w+     h+      w-      h-      shoot   select'
"""
    JOYSTICK_CONTROLLER_INFO = """
'q' - quit
'd' - enable detection mode
'l' - lose tracked object
'trig    1      2       3       4       10      11'
'trig    w+     h+      w-      h-      shoot   select'
"""