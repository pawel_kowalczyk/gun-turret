from abc import ABC, abstractmethod

class Pointer(ABC):
    """Pointer Interface"""
    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def get_corrections(self):
        pass

    @abstractmethod
    def get_clicked_buttons(self):
        pass