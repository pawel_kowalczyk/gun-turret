import os

files = []

for _, __, filename in os.walk("."):
    for name in filename:
        if name.endswith(".py"):
            files.append(name)

print(files)