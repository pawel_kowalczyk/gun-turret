import numpy as np
from numpy import sin, cos, arctan2, arccos, pi

class CoordinateTranslator:

    @staticmethod
    def sph2cart(vector:np.array):
        """
        vector = (theta(rad), phi(rad))
        """
        theta, phi = vector

        return np.array([sin(phi)*cos(theta), sin(phi)*sin(theta), cos(phi)], dtype='float64')

    @staticmethod
    def cart2sph(vector:np.array):
        """
        vector = (x(float),y(float),z(float))
        """
        x,y,z = vector

        return np.array([arctan2(y,x), arccos(z)], dtype='float64')
    
    @staticmethod
    def rot_mat(vector:np.array):
        """
        vector = (theta(rad), phi(rad))
        """
        theta, phi = vector

        rtheta = np.array([[cos(theta), -sin(theta),  0],
                           [sin(theta), cos(theta) ,  0],
                           [0         , 0          ,  1]], dtype='float64')

        rphi = np.array([[cos(phi) , 0, sin(phi)],
                         [0        , 1, 0       ],
                         [-sin(phi), 0, cos(phi)]], dtype='float64')

        return np.matmul(rphi, rtheta)

    @staticmethod
    def rotate(input_vector, origin_vector):
        """
        Returns: 
            v_out(np.array(rad)): destination vector
        """
        v_cart = CoordinateTranslator.sph2cart(input_vector)

        v_rot  = np.matmul(CoordinateTranslator.rot_mat(origin_vector), v_cart)

        v_out  = CoordinateTranslator.cart2sph(v_rot)

        return v_out

print(CoordinateTranslator.rotate([pi/2,0.2], [0,pi/2]))