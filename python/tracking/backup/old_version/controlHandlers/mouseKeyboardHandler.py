from interfaces.pointer import Pointer
from global_variables import GlobalVariables
import pygame as pg
from typing import List

class MouseKeyboardHandler(Pointer):
    DEADZONE = 0.2
    def __init__(self) -> None:
        super().__init__()

        self.pos = []
        self.speed = 0.1

    def update(self):
        self.pos = pg.mouse.get_pos()

    def get_corrections(self) -> List[float]:
        w,h = GlobalVariables.SCREEN_SIZE
        x,y = self.pos
        vector = [ 2*x/w-1, 2*y/h-1 ]
        vector = [ z if z < 1 else 1 for z in vector ]
        vector = [ 0 if abs(z) < MouseKeyboardHandler.DEADZONE else z*self.speed for z in vector ]
        
        return vector
    
    def get_clicked_buttons(self) -> List[bool]:
        """Trigger, enlargeX, reduceX, enlargeY, reduceY, change2shoot, change2track"""
                
        keys = pg.key.get_pressed()
        buttons = [keys[eval(f"pg.K_{x}")] for x in ['t',1,2,3,4,5,6]]
        return buttons