import pygame
from interfaces.pointer import Pointer
from typing import List, Dict

class JoystickHandler(Pointer):
    DEADZONE = 0.15
    def __init__(self) -> None:
        super().__init__()
        pygame.init()

        self.speed = 0.1

        self.init_joystick()

    def init_joystick(self):
        num_joysticks = pygame.joystick.get_count()
        if num_joysticks == 0:
            print("No joysticks found.")
            exit(-1)

        # Initialize the first joystick
        self.joystick = pygame.joystick.Joystick(0)
        self.joystick.init()

        print("Initialized joystick:", self.joystick.get_name())

    def _get_readings(self, axes_idx:List[int]=None, buttons_idx:List[int]=None) -> Dict[str, List[bool]]:
        """
            Args:
                axes_idx(List[int]): indexes of axes to get readings from
                buttons_idx(List[int]): indexes of buttons to get readings from

            Returns:
                Dict[str, List[bool]]: {'axes': [axes tilts], 'buttons': [pressed_buttons]}
        """
        axes = []
        buttons = []

        if axes_idx is not None:
            if max_axes_id:=max(axes_idx) >= (axes_count:=self.joystick.get_numaxes()):
                print(f"Axes index out of range {max_axes_id} >= {axes_count}")
                exit(-1)

            axes = [self.joystick.get_axis(i) for i in axes_idx]

        if buttons_idx is not None:
            if max_btn_id:=max(buttons_idx) >= (btn_count:=self.joystick.get_numbuttons()):
                print(f"Axes index out of range {max_btn_id} >= {btn_count}")
                exit(-1)

            buttons = [self.joystick.get_button(i) for i in buttons_idx]

        return {'axes':axes, 'buttons':buttons} 

    def update(self) -> None:
        """Process events"""
        pygame.event.pump()  

    def get_corrections(self) -> List[float]:
        """
            Returns:
                List[bool]: tilts of axes 
        """
        axes = self._get_readings(axes_idx=[0,1]).get("axes")
        
        for i, axis in enumerate(axes):
            if abs(axis) < JoystickHandler.DEADZONE:
                axes[i] = 0
        return [axis*self.speed for axis in axes]

    def get_clicked_buttons(self) -> List[bool]:
        """Trigger, enlargeX, reduceX, enlargeY, reduceY, change2shoot, change2track"""
        return self._get_readings(buttons_idx=[0,4,3,2,1,10,9]).get("buttons")

    def __del__(self):
        self.joystick.quit()