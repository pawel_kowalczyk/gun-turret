#pip install opencv-python opencv-contrib-python numpy pyserial pygame

from turret import Turret
from global_variables import GlobalVariables, Controllers

def main():
    GlobalVariables.DUMMY_MODE = True
    GlobalVariables.CAMERA_PORT = 0
    GlobalVariables.COMMUNICATION_PORT = 4
    GlobalVariables.CONTROLLER = Controllers.MOUSE_KEYBOARD
    
    gun = Turret()

    gun.tracking_loop()

    del gun

if __name__ == "__main__":
    main()