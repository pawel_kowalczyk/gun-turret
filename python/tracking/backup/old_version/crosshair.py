class Crosshair:
    def __init__(self) -> None:
        self.size = [60,60]

    def change_size(self, x:int, y:int):
        self.size[0] += x
        self.size[1] += y
