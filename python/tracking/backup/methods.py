# self.target.color = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)[y][x]
# color = self.target.color
# self.target.lower_color = np.array([color[0]-20, color[1]-70, color[2]-70])
# self.target.higher_color = np.array([color[0]+20, color[1]+70, color[2]+70])

# self.color = None
# self.lower_color = None
# self.higher_color = None

# def find_lost_target(self):
#     if not self.object_selected:
#         print("!Object was not selected!")
#         return False
    
#     frame = self.current_frame
#     lower = self.target.lower_color
#     higher = self.target.higher_color

#     # blured = cv2.GaussianBlur(frame, (5,5), 0)
#     frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
#     mask = cv2.inRange(frame_hsv, lower, higher)
#     masked = cv2.bitwise_and(frame_hsv, frame_hsv, mask=mask)

#     frame_bgr = cv2.cvtColor(masked, cv2.COLOR_HSV2BGR)
#     gray = cv2.cvtColor(frame_bgr, cv2.COLOR_BGR2GRAY)

#     contours, _ = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#     contours = list(i for i in contours if cv2.contourArea(i)>50)

#     if len(contours) > 0:

#         largest_contour = contours[0]
#         for c in contours:
#             if cv2.contourArea(c) > cv2.contourArea(largest_contour):
#                 largest_contour = c

#         M = cv2.moments(largest_contour)

#         if M["m00"] != 0:
#             cx = int(M["m10"]/M["m00"])
#             cy = int(M["m01"]/M["m00"])
            
#             bw, bh = self.crosshair.size
#             bbox = (int(cx-bw/2), int(cy-bh/2), bw, bh)
#             self.tracker.init(frame, bbox)
#             return True
#     return False