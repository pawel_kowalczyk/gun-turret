import socket
from connection.connection import Connection

class TcpConnection(Connection):
    def __init__(self, ip, port):
        # Define the server's IP address and port
        self.server_ip = "192.168.4.1"  # Replace with the server's IP address
        self.server_port = 6000      # Replace with the server's port
        self.client_socket = None

    def connect(self):
        # Create a TCP socket
        print("Waiting for connection...")
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Connected successfully")
        
        # Connect to the server
        self.client_socket.connect((self.server_ip, self.server_port))

    def write(self, message):
        # Send data to the server
        self.client_socket.send(message.encode())

    def read(self, length:int = 100):
        # Receive data from the server
        data = self.client_socket.recv(length)
        print("Received from server: ", data.decode())

    def close_connection(self):
        # Close the socket
        self.client_socket.close()

    def __del__(self):
        self.close_connection()