from connection.serialBluetoothConnection import SerialBluetoothConnection
from connection.serialDummyConnection import SerialDummyConnection
from connection.serialConnection import SerialConnection
from connection.tcpConnection import TcpConnection
from global_variables import *

class TurretCommunication:
    def __init__(self, port_n:int) -> None:
        """
            Args:
                port_n (int): port for serial connection
        """
        super().__init__()
        if GlobalVariables.DUMMY_MODE:
            self.connection = SerialDummyConnection(0)#SerialBluetoothConnection(port_n) #TcpConnection(None, None)
        else:
            self.connection = SerialBluetoothConnection(port_n) #TcpConnection(None, None)

        self.connection.connect()

    def send(self, x:float, y:float, s:int, cam:int, mode:int):
        """
            Args:
                x(float) : x axis correction
                y(float) : y axis correction
                s(bool)  : shot
                cam(int) : selected camera
        """
        x=round(x,3)
        y=round(y,3)
        self.connection.write(f"x{x}y{y}s{s}c{cam}m{mode}\n")

    def send_headset(self, x, y, z, w):
        """Only four values for quaternion, no shooting"""
        x=round(x,2)
        y=round(y,2)
        z=round(z,2)
        w=round(w,2)

        self.connection.write(f"h{x},{y},{z},{w}\n")
        
    # def move_by(self, x:float, y:float):
    #     x=round(x,2)
    #     y=round(y,2)
    #     self.connection.write(f"x{x}y{y}\n")
       
    def stop(self):
        """Sends corrections(0,0) to stop the turret"""
        self.connection.write("x0y0s0\n")

    def __del__(self):
        self.stop()
        del self.connection