from input_handlers.controlHandler import ControlHandler
from global_variables import GlobalVariables
import pygame as pg
from typing import List

class MouseKeyboardHandler(ControlHandler):
    DEADZONE = 0.2
    def __init__(self) -> None:
        super().__init__()

        self.pos = []
        self.speed = 0.1
        self.keys = []

    def update(self):
        self.pos = pg.mouse.get_pos()
        self.keys = pg.key.get_pressed()
        
    def get_corrections(self) -> List[float]:
        w,h = GlobalVariables.SCREEN_SIZE
        x,y = self.pos
        vector = [ 2*x/w-1, 2*y/h-1 ]
        vector = [ z if z < 1 else 1 for z in vector ]
        vector = [ 0 if abs(z) < MouseKeyboardHandler.DEADZONE else z*self.speed for z in vector ]
        
        return vector
    
    def get_control_buttons(self) -> List[bool]:
        """Trigger, Select, EnlargeXY, ReduceXY, Cam1, Cam2"""
                
        return [self.keys[eval(f"pg.K_{x}")] for x in ['t','s',1,2,3,4]]
    
    def get_keys(self):
        return self.keys