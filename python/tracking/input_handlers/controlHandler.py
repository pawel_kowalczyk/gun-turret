from abc import ABC, abstractmethod

class ControlHandler(ABC):
    """Pointer Interface"""
    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def get_corrections(self):
        pass

    @abstractmethod
    def get_control_buttons(self):
        pass