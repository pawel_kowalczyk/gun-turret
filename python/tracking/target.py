from typing import Tuple

class Target:
    """Target selected by the tracker"""
    def __init__(self, initial_pos = (0,0)) -> None:
        self.position = initial_pos
         
    def update_coords(self, x, y) -> None:
        self.position = (x,y)

    def get_coords(self) -> Tuple[float]:
        return self.position