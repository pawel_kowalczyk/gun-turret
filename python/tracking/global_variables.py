from enum import Enum

class Controllers(Enum):
    MOUSE_KEYBOARD = 0
    JOYSTICK       = 1
    HEADSET        = 2

class GlobalVariables:
    DUMMY_MODE = False
    LOOP_PERIOD = 30 #ms
    CAMERA_PORT = 0
    COMMUNICATION_PORT = 0
    CONTROLLER = Controllers.MOUSE_KEYBOARD
    SCREEN_SIZE = (640,480)
    INPUT_COEFFICIENT = 0.7
    ON_TARGET_LOCK_TIME = 99999*1000#ms
    TARGET_LOCK_DISPLACEMENT = 0.02
    KEYBOARD_CONTROLLER_INFO = """
'q' - quit
'd' - enable detection mode
'l' - lose tracked object
t       s      1       2       3       4 
trig    select s+      s-      cam1    cam2
"""
    JOYSTICK_CONTROLLER_INFO = """
'q' - quit
'd' - enable detection mode
'l' - lose tracked object
trig    3      4       6       6       7  
trig    select s+      s-      cam1    cam2
"""