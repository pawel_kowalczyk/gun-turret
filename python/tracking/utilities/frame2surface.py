import cv2
import pygame
from time import time

def frame2surface(frame) -> pygame.Surface:
    """
        Args:
            frame(cv2 frame)
        Returns:
            surface(pygame.Surface)
        
        Converts (cv2 frame) into (pygame.Surface) with BRG to RGB color change for pygame display 
    """
    
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame_rgb = cv2.flip(frame_rgb, 1)
    rotated = cv2.rotate(frame_rgb, cv2.ROTATE_90_COUNTERCLOCKWISE)
    surface = pygame.surfarray.make_surface(rotated)

    return surface 