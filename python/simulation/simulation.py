import pygame
from pygame.locals import *

import OpenGL.GL as GL
import OpenGL.GLU as GLU

from pyquaternion import Quaternion
import quat_func

import math
import numpy as np


class SceneObject:
    def __init__(self, base_orientation=(0, 0, 0, 1), base_position=(0, 0, 0), color=(1, 1, 1)) -> None:
        self.position = [0, 0, 0]
        self.rotation = Quaternion()
        self.base_orientation = base_orientation
        self.base_position = base_position
        self.texture = None
        self.color = color
        self.has_texture = False

    def load_texture(self, path):
        self.color = [1, 1, 1]
        self.has_texture = True
        texture_surface = pygame.image.load(path)
        texture_data = pygame.image.tostring(texture_surface, "RGB", True)
        width, height = texture_surface.get_size()
        self.texture = GL.glGenTextures(1)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture)
        GL.glTexParameteri(
            GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
        GL.glTexParameteri(
            GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
        GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, width, height,
                        0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, texture_data)

    def set_color(self, color):
        self.texture = None
        self.has_texture = False
        self.color = color

    def generate_object(self):
        pass

    def display(self):
        GL.glColor3f(*self.color)
        if self.texture is not None:
            GL.glBindTexture(GL.GL_TEXTURE_2D, self.texture)
        GL.glPushMatrix()

        GL.glTranslated(*self.position)
        GL.glMultMatrixd(self.rotation.conjugate.transformation_matrix)
        GL.glRotated(*self.base_orientation)
        GL.glTranslated(*self.base_position)

        self.generate_object()
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        GL.glPopMatrix()


class Sphere(SceneObject):
    def __init__(self, radius, subdivisions=50, base_orientation=(0, 0, 0, 1), base_position=(0, 0, 0),
                 color=(1, 1, 1)) -> None:
        super().__init__(base_orientation, base_position, color)
        self.radius = radius
        self.subdivisions = subdivisions

    def generate_object(self):
        quadric = GLU.gluNewQuadric()
        GLU.gluQuadricTexture(quadric, self.has_texture)
        GLU.gluQuadricDrawStyle(quadric, GLU.GLU_FILL)
        GLU.gluSphere(quadric, self.radius,
                      self.subdivisions, self.subdivisions)
        GLU.gluDeleteQuadric(quadric)


class View(SceneObject):
    def __init__(self, fov, end_x, start=(0, 0, 0), base_orientation=(0, 0, 0, 1), base_position=(0, 0, 0),
                 color=(1, 1, 1), lines_color=None) -> None:
        super().__init__(base_orientation, base_position, color)
        self.fov = fov
        self.start = start
        self.end_x = end_x
        self.lines_color = lines_color
        self.dy = math.tan(self.fov[0] * math.pi / 360) * self.end_x
        self.dz = math.tan(self.fov[1] * math.pi / 360) * self.end_x

    def generate_object(self):
        self.dy = math.tan(self.fov[0] * math.pi / 360) * self.end_x
        self.dz = math.tan(self.fov[1] * math.pi / 360) * self.end_x
        GL.glBegin(GL.GL_TRIANGLE_FAN)
        GL.glVertex3f(*self.start)
        GL.glVertex3f(self.end_x, self.dy, self.dz)
        GL.glVertex3f(self.end_x, -self.dy, self.dz)
        GL.glVertex3f(self.end_x, -self.dy, -self.dz)
        GL.glVertex3f(self.end_x, self.dy, -self.dz)
        GL.glVertex3f(self.end_x, self.dy, self.dz)
        GL.glEnd()

        # if self.lines_color is not None:
        #     GL.glColor3f(*self.lines_color)
        #     GL.glBegin(GL.GL_LINES)
        #     GL.glVertex3f(self.end_x, self.dy, self.dz)
        #     GL.glVertex3f(self.end_x, -self.dy, self.dz)
        #     GL.glVertex3f(self.end_x, -self.dy, -self.dz)
        #     GL.glVertex3f(self.end_x, self.dy, -self.dz)
        #     GL.glVertex3f(self.end_x, self.dy, self.dz)
        #     GL.glEnd()


class Simulation:
    def __init__(self, display, pos_keybinds, sprint_key, view_speed=0.02, sprint_multiplier=2, rotate_speed=0.5,
                 view_keybinds=None, use_mouse=True) -> None:
        self.display = display
        self.use_mouse = use_mouse or view_keybinds is None
        self.pos_keybinds = pos_keybinds
        self.view_keybinds = view_keybinds
        self.view_speed = view_speed
        self.sprint_multiplier = sprint_multiplier
        self.rotate_speed = rotate_speed
        self.sprint_key = sprint_key

        self.speed = self.view_speed
        self.key_callbacks = {}
        self.objects = []
        self.view_angles = np.zeros(2)
        self.view_pos = np.zeros(3)

    def pygame_init(self):
        pygame.init()
        pygame.display.set_mode(self.display, DOUBLEBUF | OPENGL)
        if self.use_mouse:
            pygame.mouse.set_visible(False)
            pygame.event.set_grab(True)

    def init_GL(self, camera_angle):
        GLU.gluPerspective(
            camera_angle, (self.display[0] / self.display[1]), 0.1, 50.0)

        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glEnable(GL.GL_TEXTURE_2D)
        GL.glMatrixMode(GL.GL_MODELVIEW)
        GL.glRotate(90, 0, 1, 0)
        GL.glRotate(-90, 1, 0, 0)
        GL.glScale(1, -1, 1)

    @staticmethod
    def read_keys(key_list, speed):
        out = np.zeros(len(key_list))
        keys = pygame.key.get_pressed()
        for index, key_pair in enumerate(key_list):
            if keys[key_pair[0]]:
                out[index] += speed
            if keys[key_pair[1]]:
                out[index] -= speed

        return out

    def add_key_callback(self, key, func, *args):
        self.key_callbacks[key] = (func, args)

    def run_callbacks(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in self.key_callbacks.keys():
                    f, args = self.key_callbacks[event.key]
                    f(*args)
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

    def set_sprint(self, sprint_key):
        self.speed = self.view_speed
        if pygame.key.get_pressed()[sprint_key]:
            self.speed *= self.sprint_multiplier

    def rotate_camera(self, spd=0.5):
        if self.use_mouse:
            view_angles_rel = pygame.mouse.get_rel()

            self.view_angles[1] += (view_angles_rel[1] / self.display[1]) * 180
            self.view_angles[0] += (view_angles_rel[0] / self.display[0]) * 180
        else:
            self.view_angles += self.read_keys(self.view_keybinds, spd)  # ((K_UP, K_DOWN), (K_LEFT, K_RIGHT))

        self.view_angles[1] = np.clip(self.view_angles[1], -90, 90)

        GL.glRotate(self.view_angles[1], 0, -1, 0)
        GL.glRotate(self.view_angles[0], 0, 0, -1)

    def move_camera(self):
        delta_pos = self.read_keys(self.pos_keybinds, self.speed)  # ((K_w, K_s), (K_a, K_d), (K_SPACE, K_LCTRL))
        self.view_pos += quat_func.qfa(self.view_angles[0] * (math.pi / 180),
                                       self.view_angles[1] * (math.pi / 180), 0).rotate(delta_pos)

        GL.glTranslated(*self.view_pos)

    def add_object(self, obj: SceneObject):
        self.objects.append(obj)

    def start_frame(self):
        GL.glPushMatrix()
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

    def end_frame(self):
        GL.glPopMatrix()


class FrameRunner(Simulation):
    def __init__(self, display, pos_keybinds, sprint_key, view_speed=0.02, sprint_multiplier=2, rotate_speed=0.5,
                 view_keybinds=None, use_mouse=True, init_pygame=True) -> None:
        super().__init__(display, pos_keybinds, sprint_key, view_speed, sprint_multiplier, rotate_speed, view_keybinds,
                         use_mouse)
        self.object_logic = None
        self.init_pygame = init_pygame
        self.sprint_key = sprint_key
        if self.init_pygame:
            self.pygame_init()
        self.init_GL(45)

    def set_object_logic(self, func):
        self.object_logic = func

    def run_frame(self):
        self.run_callbacks()
        self.start_frame()
        self.rotate_camera()
        self.set_sprint(self.sprint_key)
        self.move_camera()
        if self.object_logic is not None:
            self.object_logic()
        for obj in self.objects:
            obj.display()
        self.end_frame()

    def run_loop(self):
        while True:
            self.run_frame()
            pygame.display.flip()
            pygame.time.wait(10)
