import pygame
from pygame.locals import *
import OpenGL.GL as GL
import OpenGL.GLU as GLU
from pyquaternion import Quaternion  # Install this library with 'pip install pyquaternion'
import math
from gun import Gun

# Initialize Pygame
pygame.init()
display = (800, 600)
pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

# Set up the perspect
GLU.gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)
#GLU.gluOrtho2D(0, 10, 0, 10)

# Enable depth testing
GL.glEnable(GL.GL_DEPTH_TEST)

# Load a texture image
texture_surface = pygame.image.load("texture.png")  # Replace "texture.jpg" with your image file
texture_data = pygame.image.tostring(texture_surface, "RGB", 1)
width, height = texture_surface.get_size()

# Generate a texture ID and bind the texture
texture_id = GL.glGenTextures(1)
GL.glBindTexture(GL.GL_TEXTURE_2D, texture_id)
GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
GL.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
GL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGB, width, height, 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, texture_data)

# Initialize the quaternion for rotation
rotation_quaternion = Quaternion()
GL.glRotate(90, 0, 1, 0)
GL.glRotate(90, 1, 0, 0)
GL.glTranslated(0, 0, 0)


def qfa(yaw, pitch, roll):
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    w = cy * cp * cr + sy * sp * sr
    x = cy * cp * sr - sy * sp * cr
    y = sy * cp * sr + cy * sp * cr
    z = sy * cp * cr - cy * sp * sr

    return Quaternion([w, x, y, z])


def afq(q):
    w, x, y, z = q.elements

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return yaw_z, pitch_y, roll_x  # in radians


def afv(v):
    pitch = -math.asin(v[2])  # Pitch (angle with respect to XY-plane)
    yaw = math.atan2(v[1], v[0])  # Yaw (angle with respect to XZ-plane)

    return yaw, pitch



# Main rendering loop
q = Quaternion()
q_v = Quaternion()

view_q = Quaternion()
base_q = Quaternion()
base_v = [1, 0, 0]

view_angles = [0, 0]
view_pos = [0,0,0]

GL.glMatrixMode( GL.GL_MODELVIEW )

GL.glPushMatrix()
GL.glLoadIdentity()
trans_mat = GL.glGetFloat(GL.GL_MODELVIEW_MATRIX)
GL.glPopMatrix()

old_target_q = Quaternion()

in_pitch = 0
in_yaw = 0
out_pitch = math.pi/6
out_yaw = math.pi/4

track = True

pygame.mouse.set_visible(False)
pygame.event.set_grab(True)

while True:
    #object code
    keys = pygame.key.get_pressed()
    if keys[pygame.K_KP8]:
        in_pitch += 0.01
    if keys[pygame.K_KP2]:
        in_pitch += -0.01
    if keys[pygame.K_KP4]:
        in_yaw += -0.01
    if keys[pygame.K_KP6]:
        in_yaw += 0.01

    base_pitch = 0
    base_yaw = 0
    base_roll = 0

    if keys[pygame.K_y]:
        base_pitch = 0.01
    if keys[pygame.K_h]:
        base_pitch = -0.01
    if keys[pygame.K_g]:
        base_yaw = -0.01
    if keys[pygame.K_j]:
        base_yaw = 0.01
    if keys[pygame.K_t]:
        base_roll = -0.01
    if keys[pygame.K_u]:
        base_roll = 0.01

    base_q *= qfa(base_yaw, base_pitch, base_roll)

    if track:
        target_q = qfa(in_yaw, in_pitch, 0).conjugate
        out_yaw, out_pitch = afv((target_q*base_q).conjugate.rotate([1,0,0]))

    view_q = qfa(out_yaw, out_pitch, 0).conjugate*base_q.conjugate

    if not track:
        target_q = qfa(in_yaw, in_pitch, 0).conjugate*view_q

    if keys[pygame.K_RETURN]:
        old_target_q = target_q
        out_yaw, out_pitch = afv((target_q*base_q).conjugate.rotate([1,0,0]))
        in_yaw = 0
        in_pitch = 0

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            # Disable depth testing before exiting
            quit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                quit()
            if event.key == pygame.K_p:
                track = not track
                old_target_q = target_q
                if track:
                    in_yaw, in_pitch = afv((target_q).conjugate.rotate([1,0,0]))
                else:
                    in_yaw = 0
                    in_pitch = 0
    #View code

    # if keys[pygame.K_UP]:
    #     view_angles[0]+=0.5
    # if keys[pygame.K_DOWN]:
    #     view_angles[0]-=0.5
    # if keys[pygame.K_RIGHT]:
    #     view_angles[1]+=0.5
    # if keys[pygame.K_LEFT]:
    #     view_angles[1]-=0.5

    view_angles_rel = pygame.mouse.get_rel()

    view_angles[1] -= (view_angles_rel[1]/display[0])*180
    view_angles[0] += (view_angles_rel[0]/display[1])*180

    view_angles[1] = max((view_angles[1], -90))
    view_angles[1] = min((view_angles[1], 90))

    view_pos = [0,0,0]
    speed = 0.04 if keys[pygame.K_LSHIFT] else 0.02
    if keys[pygame.K_w]:
        view_pos[0] = -speed
    if keys[pygame.K_s]:
        view_pos[0] = speed
    if keys[pygame.K_a]:
        view_pos[1] = speed
    if keys[pygame.K_d]:
        view_pos[1] = -speed
    if keys[pygame.K_SPACE]:
        view_pos[2] = speed
    if keys[pygame.K_LCTRL]:
        view_pos[2] = -speed

    GL.glPushMatrix()
    GL.glRotate(view_angles[1], 0, -1, 0)
    GL.glRotate(view_angles[0], 0, 0, -1)

    GL.glPushMatrix()
    GL.glLoadMatrixd(trans_mat)

    view_pos = qfa(view_angles[0]*(math.pi/180), view_angles[1]*(math.pi/180), 0).rotate(view_pos)
    GL.glTranslated(*view_pos)
    trans_mat = GL.glGetFloat(GL.GL_MODELVIEW_MATRIX)
    GL.glPopMatrix()
    GL.glMultMatrixd(trans_mat)

    GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

    # Enable texturing
    GL.glEnable(GL.GL_TEXTURE_2D)

    # Bind the texture to the sphere
    GL.glBindTexture(GL.GL_TEXTURE_2D, texture_id)

    GL.glColor3f(1.0, 1.0, 1.0)  # Set color to white (for the texture)
    GL.glPushMatrix()
    GL.glMultMatrixd(base_q.conjugate.transformation_matrix)
    GL.glTranslatef(0, 0, 0)
    GL.glRotated(180, 1, 0, 0)
    GL.glRotated(90, 0, 0, 1)
    GL.glBindTexture(GL.GL_TEXTURE_2D, texture_id)
    quadric = GLU.gluNewQuadric()
    GLU.gluQuadricTexture(quadric, GL.GL_TRUE)
    GLU.gluQuadricDrawStyle(quadric, GLU.GLU_FILL)
    GLU.gluSphere(quadric, 1, 50, 50)
    GLU.gluDeleteQuadric(quadric)
    GL.glPopMatrix()

    # Draw the non-textured, blue ball at its coordinates
    GL.glColor3f(1.0, 0.0, 0.0)  # Blue color
    GL.glPushMatrix()
    #GL.glRotated(90, 1, 0, 0)
    GL.glMultMatrixd(target_q.transformation_matrix)
    GL.glTranslatef(1, 0, 0)
    quadric = GLU.gluNewQuadric()
    GLU.gluQuadricDrawStyle(quadric, GLU.GLU_FILL)
    GLU.gluSphere(quadric, 0.01, 30, 30)
    GLU.gluDeleteQuadric(quadric)
    GL.glPopMatrix()

    GL.glColor3f(0.0, 0.0, 1.0)  # Blue color
    GL.glPushMatrix()
    # GL.glRotated(90, 1, 0, 0)
    GL.glMultMatrixd(old_target_q.transformation_matrix)
    if not track:
        GL.glTranslatef(1, 0, 0)
    quadric = GLU.gluNewQuadric()
    GLU.gluQuadricDrawStyle(quadric, GLU.GLU_FILL)
    GLU.gluSphere(quadric, 0.01, 30, 30)
    GLU.gluDeleteQuadric(quadric)
    GL.glPopMatrix()

    GL.glColor3f(0.0, 0.0, 1.0)  # Blue color
    GL.glPushMatrix()
    #GL.glRotated(90, 1, 0, 0)
    GL.glMultMatrixd(view_q.transformation_matrix)
    GL.glBegin(GL.GL_TRIANGLE_FAN)
    GL.glVertex3f(0, 0, 0)
    GL.glVertex3f(1, 0.2, 0.2)
    GL.glVertex3f(1, -0.2, 0.2)
    GL.glVertex3f(1, -0.2, -0.2)
    GL.glVertex3f(1, 0.2, -0.2)
    GL.glVertex3f(1, 0.2, 0.2)
    GL.glEnd()

    Gun.create_gun()
    GL.glPopMatrix()

    GL.glPopMatrix()

    pygame.display.flip()
    pygame.time.wait(10)