from cuboid import Cuboid
from typing import Tuple
from array import array
class Gun:

    @staticmethod
    def create_gun(translation:Tuple[float]=(0,0,0)):
        Cuboid.create_cuboid(translation=(-5,0,0), scale=(0.2,0.2,0.5), rotation=(0,0,0))
        Cuboid.create_cuboid(translation=(-4,0,-0.5), scale=(0.5,0.2,0.2), rotation=(0,0,0))