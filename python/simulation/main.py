import math
import quat_func
from simulation import FrameRunner, Simulation, Sphere, View

import pygame
from pygame.locals import *
import numpy as np

USE_SERIAL = False

if USE_SERIAL:
    import serial

    ser = serial.Serial('COM6', 115200)


def end():
    pygame.quit()
    quit()


fr = FrameRunner((800, 600), ((K_s, K_w), (K_a, K_d), (K_LCTRL, K_SPACE)), K_LSHIFT)
fr.add_key_callback(K_ESCAPE, end)

base = Sphere(1, base_orientation=[90, 0, 0, 1])
base.load_texture('texture.png')
fr.add_object(base)

target = Sphere(0.01, 30, base_position=[1, 0, 0], color=[1, 0, 0])
fr.add_object(target)

old_target = Sphere(0.01, 30, base_position=[1, 0, 0], color=[0, 0, 1])
fr.add_object(old_target)

view = View((120, 85), 1, color=[0, 0, 1], lines_color=[0, 0, 0])
fr.add_object(view)

track = True

in_angle = np.zeros(2)
out_angle = np.zeros(2)


def change_track():
    global in_angle, track, out_angle
    if not USE_SERIAL:
        track = not track
        old_target.rotation = target.rotation
        if track:
            in_angle = np.array(quat_func.afv(target.rotation.rotate([1, 0, 0])))
        else:
            in_angle = np.zeros(2)


def logic():
    global in_angle, track, out_angle
    in_angle += Simulation.read_keys(((K_KP6, K_KP4), (K_KP2, K_KP8)), 0.01)
    base.rotation *= quat_func.qfa(*Simulation.read_keys(((K_j, K_g), (K_h, K_y), (K_t, K_u)), 0.01))

    view.rotation = base.rotation * quat_func.qfa(out_angle[0], out_angle[1], 0)

    if track:
        target.rotation = quat_func.qfa(*in_angle, 0)
        old_target.base_position = [0, 0, 0]
        out_angle = np.array(quat_func.afv((base.rotation.conjugate * target.rotation).rotate([1, 0, 0])))
    else:
        old_target.base_position = [1, 0, 0]
        fov = np.array(view.fov)
        in_angle = np.clip(in_angle, -fov * math.pi / 360, fov * math.pi / 360)

        target.rotation = view.rotation * quat_func.qfa(
            *quat_func.afv([1, math.tan(in_angle[0]), -math.tan(in_angle[1])]), 0)

    if pygame.key.get_pressed()[K_RETURN]:
        old_target.rotation = target.rotation
        out_angle = np.array(quat_func.afv((base.rotation.conjugate * target.rotation).rotate([1, 0, 0])))
        in_angle = np.zeros(2)


def logic_serial():
    view.rotation, base.rotation, target.rotation = quat_func.read_quaternions(ser)


fr.add_key_callback(K_p, change_track)
if USE_SERIAL:
    fr.set_object_logic(logic_serial)
else:
    fr.set_object_logic(logic)
fr.run_loop()
