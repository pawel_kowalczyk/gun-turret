import OpenGL.GL as GL
import OpenGL.GLU as GLU
from typing import Tuple

class Cuboid:
    verticies = (
        (  1, 1, 1),
        (  1,-1, 1),
        ( -1,-1, 1),
        ( -1, 1, 1),
        (  1, 1,-1),
        (  1,-1,-1),
        ( -1,-1,-1),
        ( -1, 1,-1)
    )
    edges = (
        ( 0, 1, 2, 3),
        ( 4, 5, 6, 7),
        ( 0, 1, 5, 4),
        ( 2, 3, 7, 6),
        ( 0, 3, 7, 4),
        ( 1, 2, 6, 5)
    )

    @staticmethod
    def create_cuboid(translation:Tuple[float] = (0,0,0), rotation:Tuple[float] = (0,0,0), scale:Tuple[float] = (1,1,1)):
        GL.glPushMatrix()
        GL.glTranslatef(*translation)
        GL.glScalef(*scale)
        GL.glRotatef(rotation[0], 1, 0, 0)
        GL.glRotatef(rotation[1], 0, 1, 0)
        GL.glRotatef(rotation[2], 0, 0, 1)
        GL.glBegin(GL.GL_QUADS)
        for edge in Cuboid.edges:
            GL.glColor((1,0,0))
            for vertex in edge:
                GL.glVertex3fv(Cuboid.verticies[vertex])
        GL.glEnd()
        GL.glPopMatrix()