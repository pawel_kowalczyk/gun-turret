import math
from pyquaternion import Quaternion
import serial
import numpy as np


def qfa(yaw, pitch, roll):
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    w = cy * cp * cr + sy * sp * sr
    x = cy * cp * sr - sy * sp * cr
    y = sy * cp * sr + cy * sp * cr
    z = sy * cp * cr - cy * sp * sr

    return Quaternion([w, x, y, z])


def afq(q):
    w, x, y, z = q.elements

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return yaw_z, pitch_y, roll_x  # in radians


def afv(v):
    v = np.array(v)
    v /= np.linalg.norm(v)
    pitch = -math.asin(v[2])  # Pitch (angle with respect to XY-plane)
    yaw = math.atan2(v[1], v[0])  # Yaw (angle with respect to XZ-plane)

    return yaw, pitch


def read_quaternions(serial_port: serial.Serial):
    data = serial_port.readline().decode('utf-8').strip()

    quaternion_strings = data.split(';')
    quaternions = [Quaternion(*map(float, q.split(','))) for q in quaternion_strings]

    return quaternions
