double deltat = 0.0;   

uint32_t lastUpdate1 = 0, lastUpdate2 = 0;                          // used to calculate integration interval
uint32_t Now = 0;                                 // used to calculate integration interval
  
Quaternion target_q1, target_q2(1.00,0.001,0.001,0.00), old_target_q, target_q;
Quaternion delayedQ1, prev_target_q(1.00, -0.001, -0.001,0.00);

unsigned long prev_tar = 0, prevGyroT1 = 0, prevGyroT2 = 0, interpolation_t = 0, derivativeTime = 0;
float prevGX1 = 0, prevGY1 = 0, prevGZ1 = 0, prevGX2 = 0, prevGY2 = 0, prevGZ2 = 0;
float prevAX1 = 0, prevAY1 = 0, prevAZ1 = 0, prevAX2 = 0, prevAY2 = 0, prevAZ2 = 0;
float gx1, gy1, gz1, ax1, ay1, az1;
float gx2, gy2, gz2, ax2, ay2, az2;

float prevDerivativeY = 0, prevDerivativeZ = 0;
double currentInterpolation_t = 0, dis;

bool average = false, newMPUdata1 = false, newMPUdata2 = false;
Quaternion average_q1, average_q2;

TwoWire wire1 = TwoWire(0);
TwoWire wire2 = TwoWire(1);

MPU6050 mpu(wire1);
MPU6050 mpu2(wire2);

void MPUsetup(){
  wire1.begin(21, 22, 400000);
  wire1.setClock(400000);
  wire2.begin(19, 23, 400000);
  wire2.setClock(400000);

  byte status;  
  status = mpu.begin(1,1);
  Serial.print(F("MPU6050 status 1: ")); Serial.println(status);
  while(status!=0){ }
  delay(200);

  mpu2.setAddress(0x69);
  status = mpu2.begin(1,1);
  Serial.print(F("MPU6050 status 2: ")); Serial.println(status);
  while(status!=0){ }

  mpu.setAccOffsets(0, 0, 0);
  mpu2.setAccOffsets(0, 0, 0);
  mpu.setGyroOffsets(-3.43, 0, -0.7);
  mpu2.setGyroOffsets(-4.3, -0.06, -0.06);

  delay(1500);
  Serial.println(F("Calculating offsets, do not move MPU6050"));
  mpu.calcOffsets(true,false);
  mpu2.calcOffsets(true,false);
  Serial.println(mpu2.getGyroXoffset());
  Serial.println(mpu2.getGyroYoffset());
  Serial.println(mpu2.getGyroZoffset());
  Serial.println(mpu2.getAccXoffset());
  Serial.println(mpu2.getAccYoffset());
  Serial.println(mpu2.getAccZoffset());
  Serial.println("Done!\n");
}

void updateGyro(){
  if (average){
    q1 = slerp(average_q1, q1, 0.5);
    q2 = slerp(average_q2, q2, 0.5);
    average = false;
    updatePIDinputs();
    newPID = true;
  }else{
    average_q1 = q1;
    average_q2 = q2;
    average = true;
  }
}

void updateMPU1(void * pvParameters){
  while (true){
    if (micros()-prevGyroT1>gyroUpdateMs){
      prevGyroT1 = micros();
      mpu.fetchData();
      for(uint8_t i = 0; i < 10; i++) { // iterate a fixed number of times per data read cycle
        Now = micros();
        deltat = ((Now - lastUpdate1)/1000000.0f); // set integration time by time elapsed since last filter update
        lastUpdate1 = Now;
        MadgwickQuaternionUpdate(q1, mpu.getAccX(), mpu.getAccY(), mpu.getAccZ(), mpu.getGyroX()*M_PI/180.0f, mpu.getGyroY()*M_PI/180.0f, mpu.getGyroZ()*M_PI/180.0f);
      }
      // updateGyro();
      updatePIDinputs();
      newPID = true;
      vTaskDelay(0.5/portTICK_PERIOD_MS);
    }
  }
}

void updateMPU2(void * pvParameters){
  while (true){
    if (micros()-prevGyroT2>gyroUpdateMs){
      prevGyroT2 = micros();
      mpu2.fetchData();
      for(uint8_t i = 0; i < 10; i++) { // iterate a fixed number of times per data read cycle
        Now = micros();
        deltat = ((Now - lastUpdate2)/1000000.0f); // set integration time by time elapsed since last filter update
        lastUpdate2 = Now;
        MadgwickQuaternionUpdate(q2, mpu2.getAccX(), mpu2.getAccY(), mpu2.getAccZ(), mpu2.getGyroX()*M_PI/180.0f, mpu2.getGyroY()*M_PI/180.0f, mpu2.getGyroZ()*M_PI/180.0f);
      } 
      vTaskDelay(0.5/portTICK_PERIOD_MS);
    }
  }
}


void updatePIDinputs(){
  float inY, inZ, setY, setZ;
  computeQuaternions(inZ, inY, setZ, setY);
  InputY = inY*tf_in+prev_inY*(1-tf_in);
  prev_inY = InputY;
  InputZ = inZ*tf_in+prev_inZ*(1-tf_in);
  prev_inZ = InputZ;
  SetpointY = setY*tf_in+prev_setY*(1-tf_in);
  prev_setY = SetpointY;
  setZ = setZ*tf_in+prev_setZ*(1-tf_in);
  prev_setZ = setZ;

  if (InputZ - setZ>180){
    SetpointZ = setZ + 360;
  } else if (inZ - setZ<-180){
    SetpointZ = setZ - 360;
  } else{
    SetpointZ = setZ;
  }
  
}

void computeQuaternions(float &inputZ, float &inputY, float &targetZ, float &targetY){
  qQueue.push(&q1);
  // tt = micros()-prev_tar;
  // prev_tar = micros();

  if (newCorrections){
    newCorrections = false;
    if (abs(correctionYaw)>correctionDeadzone || abs(correctionPitch)>correctionDeadzone) {
      if (trackingMode == 0){
        delayedQ1 = q1;
        if (camera == 1){
          correctionYaw *= 0.3;
          correctionPitch *= 0.3;
        }
      } else{
        qQueue.peek(&delayedQ1);
      }
      VectorFloat correctionVector(1, correctionYaw, -correctionPitch);
      correctionVector.normalize();
      VectorFloat angles = afv(correctionVector);
      target_q2 = (delayedQ1.getProduct(qfa(angles.z, angles.y, 0))).getConjugate();
      target_q1 = target_q;
      dis = quaternionDistance(target_q1, target_q2);
      if (dis<0.15){ dis = 0.15;}
      interpolation_t = millis();
    }
  }
  
  // interpolation
  currentInterpolation_t = (millis()-interpolation_t)/1000.0f/correctionSpeed/dis;
  if (currentInterpolation_t < 0) {currentInterpolation_t = 0;}

  if (currentInterpolation_t < interI) {
    currentInterpolation_t = interSlope * currentInterpolation_t;
  } else{
    currentInterpolation_t = 1 - fPower(currentInterpolation_t + interM, -interCurve);
  }
  old_target_q = target_q;
  target_q = slerp(target_q1, target_q2, currentInterpolation_t);

  target_q = slerp(prev_target_q, target_q, quatFilter);
  prev_target_q = target_q;

  VectorFloat vv(1,0,0);
  Quaternion qq = target_q.getProduct(q2).getConjugate();
  VectorFloat out = afv(vv.getRotated(&qq));

  if (out.z<limitZ1 && out.z>limitZ0 && out.y<limitY1 && out.y>limitY0){  // verify if the target is within the limits set
    targetZ = out.z * 180 / M_PI;
    targetY = out.y * 180 / M_PI;
  } else{
    target_q = old_target_q;
  }

  Quaternion qqq = q1.getConjugate().getProduct(q2);
  VectorFloat ang = afq(&qqq);
  inputZ = ang.z * 180 / M_PI;
  inputY = ang.y * 180 / M_PI;

  // verify motor stall
  float gyroSpeedY = (inputY - prevDerivativeY)/(micros()-derivativeTime)*1000000.0f;
  prevDerivativeY = inputY;
  float gyroSpeedZ = (inputZ - prevDerivativeZ)/(micros()-derivativeTime)*1000000.0f;
  prevDerivativeZ = inputZ;
  derivativeTime = micros();

  if (abs(outY/6400*360-gyroSpeedY)>200){
    prev_outY = 0;
    // Serial.println("stall Y");
  }
  if (abs(outZ/6400*360-gyroSpeedZ)>200){
    prev_outZ = 0;
    // Serial.println("stall Z");
  }
}

float getAccAngle(){
  for (int i = 0; i<20; i++){
    mpu.fetchData();
  }
  return atan2(-mpu.getAccX(), sqrt(sq(mpu.getAccY()) + sq(mpu.getAccZ()))) * 180/M_PI;
}

void printQuaternion(Quaternion q, bool end){
    Serial.print(q.w, 5);
    Serial.print(",");
    Serial.print(q.x, 5);
    Serial.print(",");
    Serial.print(q.y, 5);
    Serial.print(",");
    Serial.print(q.z, 5);
    if (end){
      Serial.println();
    } else{
      Serial.print(";  ");
    }
}