BluetoothSerial bt;

float x = 0, y = 0, z = 0;
String ss;
unsigned long prev_ttt = 0;
bool shooting = false;

void communication(void * pvParameters){
  bt.begin("esp");
  delay(200);
  while (true){
    vTaskDelay(4/portTICK_PERIOD_MS);
    ss = readSerial();
    processString(ss);
    ss = readBL();
    processString(ss);
  }
}

String readSerial() {
  if (Serial.available()) {
    String s = Serial.readStringUntil('\n');
    Serial.flush();
    return s;
  } else {
    return "";
  }
}

String readBL() {
  if (bt.available()) {
    String s = bt.readStringUntil('\n');
    return s;
  } else {
    return "";
  }
}

void processString(String s){
  float param = 0;
  if (s == ""){ return; }

  if (s[0] == 'p' && s[1] == 'x') { extractFloat(s, Kp); pidZ.SetTunings(Kp, Ki, Kd); printPidCoef("pidX:", pidZ);}
  else if (s[0] == 'i' && s[1] == 'x') { extractFloat(s, Ki); pidZ.SetTunings(Kp, Ki, Kd); printPidCoef("pidX:", pidZ);}
  else if (s[0] == 'd' && s[1] == 'x') { extractFloat(s, Kd); pidZ.SetTunings(Kp, Ki, Kd); printPidCoef("pidX:", pidZ);}
  else if (s[0] == 'p' && s[1] == 'y') { extractFloat(s, kp); pidY.SetTunings(kp, ki, kd); printPidCoef("pidY:", pidY);}
  else if (s[0] == 'i' && s[1] == 'y') { extractFloat(s, ki); pidY.SetTunings(kp, ki, kd); printPidCoef("pidY:", pidY);}
  else if (s[0] == 'd' && s[1] == 'y') { extractFloat(s, kd); pidY.SetTunings(kp, ki, kd); printPidCoef("pidY:", pidY);}
  else if (s[0] == 't' && s[1] == 'a') { extractFloat(s, tf_acc2); Serial.print("tf_acc2 = "); Serial.println(tf_acc2);}
  else if (s[0] == 't' && s[1] == 'd') { extractFloat(s, tf_dacc); Serial.print("tf_dacc = "); Serial.println(tf_dacc);}
  else if (s[0] == 't' && s[1] == 'i') { extractFloat(s, tf_in); Serial.print("tf_in = "); Serial.println(tf_in);}
  else if (s[0] == 'z' && s[1] == 'z') { extractFloat(s, zeta); Serial.print("zeta = "); Serial.println(zeta);}
  else if (s[0] == 'a' && s[1] == 'y') { extractFloat(s, accelCofY); Serial.print("accelCofY = "); Serial.println(accelCofY);}
  else if (s[0] == 'a' && s[1] == 'x') { extractFloat(s, accelCofX); Serial.print("accelCofX = "); Serial.println(accelCofX);}
  else if (s[0] == 'a' && s[1] == 'i') { extractFloat(s, accelInt); Serial.print("accelInt = "); Serial.println(accelInt,6);}
  else if (s[0] == 'c' && s[1] == 's') { extractFloat(s, correctionSpeed); Serial.print("correctionSpeed = "); Serial.println(correctionSpeed);}
  else if (s[0] == 's' && s[1] == 's') { if (!shooting) {xTaskCreatePinnedToCore(shoot, "Task5", 10000, NULL, 1, &Task5, 0);} }
  else{
    if (getArgFromString(s, "c", param)){ digitalWrite(cameraPin, (bool)param); camera = (bool)param;}
    if (getArgFromString(s, "m", param)){ trackingMode = (bool)param;}
    if (getArgFromString(s, "s", param) && param>0.5 && !shooting){ xTaskCreatePinnedToCore(shoot, "Task5", 10000, NULL, 1, &Task5, 0); }
    if (getArgFromString(s, "w", qHeadTrack.w) && getArgFromString(s, "x", qHeadTrack.x) && getArgFromString(s, "y", qHeadTrack.y) && getArgFromString(s, "z", qHeadTrack.z)) {
      newHeadQuaternion = true;
      headTrack_t = millis();
    } else {
      correctionPitch = 0;
      correctionYaw = 0;
      bool b1 = getArgFromString(s, "x", correctionYaw);
      bool b2 = getArgFromString(s, "y", correctionPitch);
      if (b1 || b2){
        newCorrections = true;
      }
    }
  }
}

void shoot(void * pvParameters){
  shooting = true;
  digitalWrite(gun, HIGH); 
  vTaskDelay(75/portTICK_PERIOD_MS); 
  digitalWrite(gun, LOW); 
  vTaskDelay(75/portTICK_PERIOD_MS); 
  shooting = false;
  vTaskDelete(Task5);
}

void extractFloat(String s, float &parameter){
  int i = 2;
  String m = "";

  while ((isDigit(s[i]) || s[i] == '-' || s[i] == '.') && i < s.length()) {
    m += s[i];
    i++;
  }

  if (m != "") {
    parameter = m.toFloat();
  }
}

void extractInt(String s, long &parameter){
  int i = 2;
  String m = "";

  while ((isDigit(s[i]) || s[i] == '-' || s[i] == '.') && i < s.length()) {
    m += s[i];
    i++;
  }

  if (m != "") {
    parameter = m.toInt();
  }
}

void printPidCoef(String m, PID x){
  Serial.println(m);
  Serial.println(x.GetKp());
  Serial.println(x.GetKi());
  Serial.println(x.GetKd());
}

bool getArgFromString(String s, String sf, float &param){
    int pos = -1, searchPos = 0;
    String m = "";
    bool found = false;

    // find the position of the number in the string
    while (s.indexOf(sf, searchPos) != -1){
        pos = s.indexOf(sf, searchPos) + sf.length();
        if (isdigit(s[pos]) || s[pos] == '-'){
            found = true;
            break;
        }
        searchPos = s.indexOf(sf, searchPos) + 1;
    }

    if (!found) { return false; }

    // get the full number from the string
    while ((isdigit(s[pos]) || s[pos] == '.' || s[pos] == '-') && pos<s.length()){
        m += s[pos];
        pos++;
    }
    param = m.toFloat();
    return true;
}

void stringToPosition(String s, float &xx, float &yy) {
  int i = 0, k = 0;
  if (s[0] == 'b') { i = 3; k = 2;}
  if (s[0] == 'x' || s[0] == 'y') { i = 1; k = 0;}
  String m = "";
  while ((isDigit(s[i]) || s[i] == '-' || s[i] == '.') && i < s.length()) {
    m += s[i];
    i++;
  }

  if (m != "") {
    if (s[k] == 'x') {
      xx = m.toFloat();
    } else {
      yy = m.toFloat();
    }
  }

  if (i < s.length() && (s[i] == 'x' || s[i] == 'y')) {
    int ii = i;
    m = "";
    i++;
    while ((isDigit(s[i]) || s[i] == '-' || s[i] == '.') && i < s.length()) {
      m += s[i];
      i++;
    }

    if (m != "") {
      if (s[ii] == 'x') {
        xx = m.toFloat();
      } else {
        yy = m.toFloat();
      }
    }
  }
}