// filters
float tf_in = 0.5;  // angle gyro input filter
float tf_acc1 = 0.015;  // large error acceleration filter
float tf_acc2 = 0.035;  // medium error acceleration filter
float tf_acc3 = 0.01;  // small error acceleration filter
float tf_dacc = 0.03; // deacceleration filter
float transitionAngle12 = 7; // degrees, error when the "medium error acceleration filter" is applied
float transitionAngle23 = 1; // degrees, error when the "small error acceleration filter" is applied
float accelCofX = 1;
float accelCofY = 1;
float accelInt = 0.0001;

float deadband = 0.01; // degrees, deadband for running the motors
long gyroUpdateMs = 1000; // microseconds, gyro sample rate

// correction settings
long correctionUpdateTime = 1;
float correctionDeadzone = 0.1 / 180*M_PI; // deadband for received camera corrections
float correctionSpeed = 0.4; // interpolation time scalar

// quaternion interpolation settings
float interSlope = 2; // slope of the linear part of the interpolation
float interCurve = 3.5; // curve of the exponential part of the interpolation
float interM = 0.956; // offset of the expontial part (calculated from slope and curve)
float interI = 0.176; // intersection point of the linear and exponential part (calculated form slope and curve)
float quatFilter = 0.02;

// PID loop settings
float Kp=500, Ki=40, Kd=0.4; //Z axis 
float kp=600, ki=40, kd=0.4; //Y axis

int pidSampleTimeZ = 2, pidSampleTimeY = 2;
long maxSpeedZ = 15000, maxSpeedY = 15000;

// parameters for Madgwick sensor fusion calculations
// float beta = sqrt(3.0f / 4.0f) * PI * (40.0f / 180.0f);  // rad/s drift
// float zeta = sqrt(3.0f / 4.0f) * PI * (2.0f / 180.0f);   // rad/s/s drift acceleration
float beta = 0.005; // 0.041  decrease filter gain after stabilized
float zeta = 0.005;

#define CurrentY 45 // current fot Y motor
#define CurrentZ 50 // current fot X motor

const long limitY0 = -80, limitY1 = 70, limitZ0 = -181, limitZ1 = 181; // axis limits in degrees