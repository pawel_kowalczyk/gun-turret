Quaternion slerp(Quaternion q1n, Quaternion q2n, float t) {
    q1n.normalize();
    q2n.normalize();

    float dotProduct = q1n.w * q2n.w + q1n.x * q2n.x + q1n.y * q2n.y + q1n.z * q2n.z;

    if (abs(dotProduct)<1){ 
    
    // Interpolate using Slerp
    float theta = acos(constrain(dotProduct, -1, 1));
    if (theta<0.0f){ theta = -theta;}
    float sinTheta = sin(theta);
    float weightQ1 = sin((1.0f - t) * theta) / sinTheta;
    float weightQ2 = sin(t * theta) / sinTheta;

    // Perform the interpolation
    Quaternion result;
    result.w = q1n.w*weightQ1 + q2n.w*weightQ2;
    result.x = q1n.x*weightQ1 + q2n.x*weightQ2;
    result.y = q1n.y*weightQ1 + q2n.y*weightQ2;
    result.z = q1n.z*weightQ1 + q2n.z*weightQ2;

    return result.getNormalized();
    }else{
      return q2n; 
    }
}

float quaternionDistance(const Quaternion q1n, const Quaternion q2n) {
    float dotProduct = q1n.w * q2n.w + q1n.x * q2n.x + q1n.y * q2n.y + q1n.z * q2n.z;

    float angleRadians = acos(constrain(2 * dotProduct * dotProduct - 1, -1, 1));

    // double angleRadians = (1.0 - sq(dotProduct)) * M_PI;

    return angleRadians;
}

void MadgwickQuaternionUpdate(Quaternion &q, float ax, float ay, float az, float gyrox, float gyroy, float gyroz)
{
    float q1 = q.w, q2 = q.x, q3 = q.y, q4 = q.z;         // short name local variable for readability
    float norm;                                               // vector norm
    float f1, f2, f3;                                         // objetive funcyion elements
    float J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33; // objective function Jacobian elements
    float qDot1, qDot2, qDot3, qDot4;
    float hatDot1, hatDot2, hatDot3, hatDot4;
    float gerrx, gerry, gerrz, gbiasx, gbiasy, gbiasz;        // gyro bias error

    // Auxiliary variables to avoid repeated arithmetic
    float _halfq1 = 0.5f * q1;
    float _halfq2 = 0.5f * q2;
    float _halfq3 = 0.5f * q3;
    float _halfq4 = 0.5f * q4;
    float _2q1 = 2.0f * q1;
    float _2q2 = 2.0f * q2;
    float _2q3 = 2.0f * q3;
    float _2q4 = 2.0f * q4;
    float _2q1q3 = 2.0f * q1 * q3;
    float _2q3q4 = 2.0f * q3 * q4;

    // Normalise accelerometer measurement
    norm = sqrt(ax * ax + ay * ay + az * az);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f/norm;
    ax *= norm;
    ay *= norm;
    az *= norm;
    
    // Compute the objective function and Jacobian
    f1 = _2q2 * q4 - _2q1 * q3 - ax;
    f2 = _2q1 * q2 + _2q3 * q4 - ay;
    f3 = 1.0f - _2q2 * q2 - _2q3 * q3 - az;
    J_11or24 = _2q3;
    J_12or23 = _2q4;
    J_13or22 = _2q1;
    J_14or21 = _2q2;
    J_32 = 2.0f * J_14or21;
    J_33 = 2.0f * J_11or24;
  
    // Compute the gradient (matrix multiplication)
    hatDot1 = J_14or21 * f2 - J_11or24 * f1;
    hatDot2 = J_12or23 * f1 + J_13or22 * f2 - J_32 * f3;
    hatDot3 = J_12or23 * f2 - J_33 *f3 - J_13or22 * f1;
    hatDot4 = J_14or21 * f1 + J_11or24 * f2;
    
    // Normalize the gradient
    norm = sqrt(hatDot1 * hatDot1 + hatDot2 * hatDot2 + hatDot3 * hatDot3 + hatDot4 * hatDot4);
    hatDot1 /= norm;
    hatDot2 /= norm;
    hatDot3 /= norm;
    hatDot4 /= norm;
    
    // Compute estimated gyroscope biases
    gerrx = _2q1 * hatDot2 - _2q2 * hatDot1 - _2q3 * hatDot4 + _2q4 * hatDot3;
    gerry = _2q1 * hatDot3 + _2q2 * hatDot4 - _2q3 * hatDot1 - _2q4 * hatDot2;
    gerrz = _2q1 * hatDot4 - _2q2 * hatDot3 + _2q3 * hatDot2 - _2q4 * hatDot1;
    
    // Compute and remove gyroscope biases
    gbiasx += gerrx * deltat * zeta;
    gbiasy += gerry * deltat * zeta;
    gbiasz += gerrz * deltat * zeta;
    gyrox -= gbiasx;
    gyroy -= gbiasy;
    gyroz -= gbiasz;
    
    // Compute the quaternion derivative
    qDot1 = -_halfq2 * gyrox - _halfq3 * gyroy - _halfq4 * gyroz;
    qDot2 =  _halfq1 * gyrox + _halfq3 * gyroz - _halfq4 * gyroy;
    qDot3 =  _halfq1 * gyroy - _halfq2 * gyroz + _halfq4 * gyrox;
    qDot4 =  _halfq1 * gyroz + _halfq2 * gyroy - _halfq3 * gyrox;

    // Compute then integrate estimated quaternion derivative
    q1 += (qDot1 -(beta * hatDot1)) * deltat;
    q2 += (qDot2 -(beta * hatDot2)) * deltat;
    q3 += (qDot3 -(beta * hatDot3)) * deltat;
    q4 += (qDot4 -(beta * hatDot4)) * deltat;

    // Normalize the quaternion
    norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
    norm = 1.0f/norm;
    q.w = q1 * norm;
    q.x = q2 * norm;
    q.y = q3 * norm;
    q.z = q4 * norm;
    // q.w = q1;
    // q.x = q2;
    // q.y = q3;
    // q.z = q4;
    // q.normalize();
    // dNormalize(q);
    // printQuaternion(q, true);
    // Serial.println(magnitude(q),6);
}

double fPower(double x, double n){
  return exp(n*log(x));
}

Quaternion qfa(float yaw, float pitch, float roll){
    Quaternion ret;
    float cy = cos(yaw * 0.5);
    float sy = sin(yaw * 0.5);
    float cp = cos(pitch * 0.5);
    float sp = sin(pitch * 0.5);
    float cr = cos(roll * 0.5);
    float sr = sin(roll * 0.5);

    ret.w = cy * cp * cr + sy * sp * sr;
    ret.x = cy * cp * sr - sy * sp * cr;
    ret.y = sy * cp * sr + cy * sp * cr;
    ret.z = sy * cp * cr - cy * sp * sr;

    return ret;
}

VectorFloat afq(const Quaternion* q) {
  VectorFloat v;
    v.z = atan2(2*q -> x*q -> y - 2*q -> w*q -> z, 2*q -> w*q -> w + 2*q -> x*q -> x - 1);   // psi
    v.y = -asin(2*q -> x*q -> z + 2*q -> w*q -> y);                              // theta
    v.x = atan2(2*q -> y*q -> z - 2*q -> w*q -> x, 2*q -> w*q -> w + 2*q -> z*q -> z - 1);   // phi
    return v;
}

VectorFloat afv(VectorFloat v){
  VectorFloat vv(0,0,0);
    vv.y = -asin(v.z);
    vv.z = atan2(v.y, v.x);
    return vv;
}