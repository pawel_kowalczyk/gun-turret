#define SERIAL_PORT Serial2
#define R_SENSE 0.11f
#define driverZ_ADDRESS 0b01 
#define driverY_ADDRESS 0b00
#define STALL_PIN_Z      32
#define STALL_PIN_Y      25

#define STALLZ_VALUE 1
#define STALLY_VALUE 1


HardwareSerial & serial_stream = Serial2;

const long maxSpeed = 15000, acc = 10000;

TMC2209 driverX, driverY;

void setupDrivers(){
  pinMode(enablePin, OUTPUT);
  digitalWrite(enablePin, LOW);
  mz.setMaxSpeed(maxSpeed);  my.setMaxSpeed(maxSpeed);
  mz.setAcceleration(acc);  my.setAcceleration(acc);
  driverX.setup(serial_stream, 115200, TMC2209::SERIAL_ADDRESS_1);
  driverY.setup(serial_stream, 115200, TMC2209::SERIAL_ADDRESS_0);
  driverY.setReplyDelay(4);
  driverX.setReplyDelay(4);
  driversReady = driverX.isSetupAndCommunicating() && driverY.isSetupAndCommunicating();
  driverX.setMicrostepsPerStepPowerOfTwo(5);
  driverX.enableAutomaticCurrentScaling();
  driverX.setRunCurrent(CurrentZ);
  driverX.setStandstillMode(TMC2209::NORMAL);
  driverX.setHoldCurrent(30);
  driverX.enable();

  driverY.setMicrostepsPerStepPowerOfTwo(5);
  driverY.enableAutomaticCurrentScaling();
  driverY.setRunCurrent(CurrentY);
  driverY.setStandstillMode(TMC2209::NORMAL);
  driverY.setHoldCurrent(30);
  driverY.enable();
}