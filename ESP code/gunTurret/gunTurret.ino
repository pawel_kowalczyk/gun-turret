#include <AccelStepper.h>
#include "BluetoothSerial.h"
#include <TMC2209.h>
#include <TMCStepper.h>
#include <PID_v1.h>
#include <MPU6050_light.h>
#include <Wire.h>
#include <cppQueue.h>
#include "helper_3dmath.h"
#include "settings.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"


const byte step_my = 15, dir_my = 2, enablePin = 4;
const byte step_mz = 18, dir_mz = 5;

extern AccelStepper my(AccelStepper::DRIVER, step_my, dir_my);
extern AccelStepper mz(AccelStepper::DRIVER, step_mz, dir_mz);

const byte gun = 12, cameraPin = 26;

cppQueue qQueue(sizeof(Quaternion), 50, FIFO, true);

unsigned long prev_t = 0, prev_tt = 0, tt, headTrack_t = 0, prev_tarr = 0;
float stepAngle, tfY, tfZ;

Quaternion q1, q2;

bool trackingMode = 0;
bool camera = 0;

double SetpointZ, InputZ, OutputZ;
double SetpointY, InputY, OutputY;

float prev_outZ = 0, outZ = 0, prev_inZ = 0, outZmod = 0, prev_setZ = 0;
float prev_outY = 0, outY = 0, prev_inY = 0, prev_setY = 0;

PID pidZ(&InputZ, &OutputZ, &SetpointZ, 0, 0, 0, DIRECT);
PID pidY(&InputY, &OutputY, &SetpointY, 0, 0, 0, DIRECT);

TaskHandle_t Task1 = NULL, Task2 = NULL, Task5 = NULL;

float correctionYaw = 0, correctionPitch = 0;
bool newCorrections = true, newHeadQuaternion = false;

bool newGyroUpdate = false;

bool driversReady = false, newPID = true;

long duration;

Quaternion qHeadTrack;

void setup() {
  esp_task_wdt_init(500, true);
  esp_task_wdt_add(NULL);
  Serial.begin(115200);
  xTaskCreatePinnedToCore(communication, "Task1", 10000, NULL, 1, &Task1, 0);
  setupDrivers();
  MPUsetup();
  my.runToNewPosition(-getAccAngle()*6400/360);
  xTaskCreatePinnedToCore(updateMPU1, "Task7", 10000, NULL, 2, NULL, 0);
  xTaskCreatePinnedToCore(updateMPU2, "Task8", 10000, NULL, 2, NULL, 0);
  delay(1000);

  pidZ.SetOutputLimits(-maxSpeedZ, maxSpeedZ);
  pidY.SetOutputLimits(-maxSpeedY, maxSpeedY);
  pidZ.SetSampleTime(pidSampleTimeZ);
  pidY.SetSampleTime(pidSampleTimeY);
  pidZ.SetMode(AUTOMATIC);
  pidY.SetMode(AUTOMATIC);
  pidZ.SetTunings(Kp, Ki, Kd);
  pidY.SetTunings(kp, ki, kd);

  pinMode(gun, OUTPUT);
  digitalWrite(gun, LOW);
  pinMode(cameraPin, OUTPUT);
  digitalWrite(cameraPin, LOW);

  q1.w = 1; q1.x = 0; q1.y = 0; q1.z = 0;
  q2.w = 1; q2.x = 0; q2.y = 0; q2.z = 0;
  SetpointZ = 0;
  SetpointY = 0;

  while (!driversReady){
    Serial.print(".");
    delay(2000);
  }
}

void loop() {
  delayMicroseconds(15);
 
    if (newPID){
      newPID = false;

    pidZ.Compute();
    outZmod = OutputZ * cos(InputY / 180 * M_PI);
    if (abs(OutputZ) - abs(prev_outZ) < 0) {
      tfZ = tf_dacc;
    } else if (abs(InputZ - SetpointZ) > transitionAngle12) {
      tfZ = tf_acc1;
    } else {
      tfZ = (abs(mz.speed()-outZmod))/10000.0f*accelCofX + accelInt;
      if (tfZ > 0.02f){ tfZ = 0.02f; }
    }

    if (abs(SetpointZ - InputZ) > deadband) {
      outZ = outZmod * tfZ + prev_outZ * (1 - tfZ);
      prev_outZ = outZ;
      mz.setSpeed(outZ);
    } else {
      mz.setSpeed(0);
      prev_outZ = 0;
    }

    pidY.Compute();
    if (abs(OutputY) - abs(prev_outY) < 0) {
      tfY = tf_dacc;
    } else if (abs(InputY - SetpointY) > transitionAngle12) {
      tfY = tf_acc1;
    } else {
      tfY = (abs(my.speed()-OutputY))/10000.0f*accelCofY + accelInt;
      if (tfY > 0.02f){ tfY = 0.02f; }
    }

    if (abs(SetpointY - InputY) > deadband) {
      outY = OutputY * tfY + prev_outY * (1 - tfY);
      prev_outY = outY;
      my.setSpeed(outY);
    } else {
      my.setSpeed(0);
      prev_outY = 0;
    }
  }

  my.runSpeed();
  mz.runSpeed();
}